package com.devtech.webshop.controller;

import com.devtech.webshop.model.Payment;
import com.devtech.webshop.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/payments")
public class PaymentRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentRestController.class);
    private final PaymentService paymentService;

    @Autowired
    PaymentRestController(final PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping
    public ResponseEntity<Payment> create(@RequestBody final Payment payment) {
        try {
            return new ResponseEntity<>(paymentService.create(payment), HttpStatus.CREATED);
        } catch (final Exception ex) {
            LOGGER.error("An error has occurred in create payment: {}", ex.getMessage(), ex);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{customerId}")
    public ResponseEntity<List<Payment>> getByCustomerId(@PathVariable("customerId") final Integer customerId) {
        try {
            return new ResponseEntity<>(paymentService.getByCustomerId(customerId), HttpStatus.OK);
        } catch (final Exception ex) {
            LOGGER.error("Cannot find payments for customer with ID:{}, {}", customerId, ex.getMessage(), ex);
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all/{pageNumber}")
    public ResponseEntity<Page<Payment>> getAllByPagination(@PathVariable("pageNumber") final int pageNumber) {
        try {
            return new ResponseEntity<>(paymentService.getAllByPagination(pageNumber), HttpStatus.OK);
        } catch (final Exception ex) {
            LOGGER.error("Cannot find payments with page:{}, {}", pageNumber, ex.getMessage(), ex);
            return new ResponseEntity<>(Page.empty(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
