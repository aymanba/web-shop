package com.devtech.webshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebShopApplication {

    public static void main(final String[] args) {
        SpringApplication.run(WebShopApplication.class, args);
    }

}
