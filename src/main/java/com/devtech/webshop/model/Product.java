package com.devtech.webshop.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends BaseEntity<Integer> {

    private static final long serialVersionUID = 2137883317269046863L;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "reference", nullable = false)
    private String reference;

    @Column(name = "price", nullable = false)
    private Double price;
    
    private String description;
}
