package com.devtech.webshop.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment extends BaseEntity<Integer> {

    private static final long serialVersionUID = 8217887687386188458L;

    @Column(name = "transaction_id", nullable = false)
    private UUID transactionId;
    
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createdDate;

    @ManyToOne
    @JoinColumn(name = "payment_type", nullable = false)
    private PaymentType paymentType;

    @ManyToOne
    @JoinColumn(name = "customer", nullable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "product", nullable = false)
    private Product product;

    @PrePersist
    void beforePersist() {
        this.createdDate = LocalDateTime.now();
        this.transactionId = UUID.randomUUID();
    }

}
