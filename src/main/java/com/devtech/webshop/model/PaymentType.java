package com.devtech.webshop.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "payment_type")
public class PaymentType extends BaseEntity<Byte> {

    private static final long serialVersionUID = -5537122561667737968L;

    @Column(name = "name", length = 128, nullable = false)
    private String name;
}
