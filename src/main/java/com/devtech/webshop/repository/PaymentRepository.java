package com.devtech.webshop.repository;

import com.devtech.webshop.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    List<Payment> findByCustomerId(Integer customerId);
}
