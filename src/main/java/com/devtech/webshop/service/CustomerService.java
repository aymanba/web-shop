package com.devtech.webshop.service;

import com.devtech.webshop.model.Customer;

public interface CustomerService {

    Customer create(Customer customer);
}
