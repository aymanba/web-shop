package com.devtech.webshop.service;

import com.devtech.webshop.model.Product;

public interface ProductService {

    Product create(Product product);
}
