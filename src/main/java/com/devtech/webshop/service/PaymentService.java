package com.devtech.webshop.service;

import com.devtech.webshop.model.Payment;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PaymentService {

    Payment create(Payment payment);

    List<Payment> getByCustomerId(Integer customerId);

    Page<Payment> getAllByPagination(int pageNumber);
}
