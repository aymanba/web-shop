package com.devtech.webshop.service.impl;

import com.devtech.webshop.model.Customer;
import com.devtech.webshop.repository.CustomerRepository;
import com.devtech.webshop.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    CustomerServiceImpl(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(final Customer customer) {
        Assert.notNull(customer, "Customer to save is null");
        return customerRepository.save(customer);
    }
}
