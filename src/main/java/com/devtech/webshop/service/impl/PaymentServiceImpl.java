package com.devtech.webshop.service.impl;

import com.devtech.webshop.model.Payment;
import com.devtech.webshop.repository.PaymentRepository;
import com.devtech.webshop.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

@Service("paymentService")
@Transactional
public class PaymentServiceImpl implements PaymentService {

    private static final int DEFAULT_PAGE_SIZE = 2;
    private final PaymentRepository paymentRepository;

    @Autowired
    PaymentServiceImpl(final PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Payment create(final Payment payment) {
        Assert.notNull(payment, "Payment to save is null");
        return paymentRepository.save(payment);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Payment> getByCustomerId(final Integer customerId) {
        if (customerId == null) {
            return Collections.emptyList();
        }
        return paymentRepository.findByCustomerId(customerId);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Payment> getAllByPagination(final int pageNumber) {
        return paymentRepository.findAll(PageRequest.of(pageNumber, DEFAULT_PAGE_SIZE));
    }
}
