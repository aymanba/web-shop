package com.devtech.webshop.service.impl;

import com.devtech.webshop.model.Product;
import com.devtech.webshop.repository.ProductRepository;
import com.devtech.webshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    ProductServiceImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product create(final Product product) {
        Assert.notNull(product, "Product to save is null");
        return productRepository.save(product);
    }
}
